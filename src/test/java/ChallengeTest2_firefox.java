import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.PageFactory;
import java.io.InputStream;
import java.util.Properties;

public class ChallengeTest2_firefox {
	PageForm2 maPage;
	WebDriver driver;

	@Test
	public void test() throws Exception {
		//Initialisation du driver
		FirefoxOptions firefoxOptions = new FirefoxOptions();
		firefoxOptions.addArguments("-headless", "-safe-mode");
		driver = new FirefoxDriver(firefoxOptions);
		//System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\driver\\geckodriver.exe");
		//WebDriver driver = new FirefoxDriver();

		//Méthode pour lire un fichier properties depuis un environnement maven et initialiser le fichier.properties
		final String PROPERTIES_NAME = "JDD.properties";
		ClassLoader classLoader = getClass().getClassLoader();
		try (final InputStream inputStream = classLoader.getResourceAsStream(PROPERTIES_NAME)) {
			if (inputStream == null) {
				throw new IllegalArgumentException("Resource file " + PROPERTIES_NAME + " not found");
			}
			Properties propertyJDD = new Properties();
			propertyJDD.load(inputStream);

			//aller sur le site du challenge
			driver.get(propertyJDD.getProperty("URL"));
			driver.manage().window().maximize();

			//Initialisation de ma page
			maPage = PageFactory.initElements(driver, PageForm2.class);

			// click Start et remplir les formulaires avec boucle for
			maPage.Form(driver, propertyJDD);
			driver.quit();

		}
	}
}