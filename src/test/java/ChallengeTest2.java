import java.io.InputStream;
import java.util.Properties;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;

public class ChallengeTest2 {
	PageForm2 maPage;
	WebDriver driver;

	@Test
	public void test() throws Exception {
		//Initialisation du driver
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments("--no-sandbox");
		chromeOptions.addArguments("--disable-dev-shm-usage");
		chromeOptions.addArguments("--disable-gpu");
		chromeOptions.addArguments("--headless");
		chromeOptions.setBinary("/usr/bin/chromium");
		System.setProperty("webdriver.chrome.whitelistedIps", "");
		driver = new ChromeDriver(chromeOptions);
		//System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\driver\\geckodriver.exe");
		//WebDriver driver = new FirefoxDriver();

		//Méthode pour lire un fichier properties depuis un environnement maven et initialiser le fichier.properties
		final String PROPERTIES_NAME = "JDD.properties";
		ClassLoader classLoader = getClass().getClassLoader();
		try (final InputStream inputStream = classLoader.getResourceAsStream(PROPERTIES_NAME)) {
			if (inputStream == null) {
				throw new IllegalArgumentException("Resource file " + PROPERTIES_NAME + " not found");
			}
			Properties propertyJDD = new Properties();
			propertyJDD.load(inputStream);

			//aller sur le site du challenge
			driver.get(propertyJDD.getProperty("URL"));
			driver.manage().window().maximize();

			//Initialisation de ma page
			maPage = PageFactory.initElements(driver, PageForm2.class);

			// click Start et remplir les formulaires avec boucle for
			maPage.Form(driver, propertyJDD);
			driver.quit();

		}
	}
}